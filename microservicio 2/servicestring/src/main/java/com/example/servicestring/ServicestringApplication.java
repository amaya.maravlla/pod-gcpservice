package com.example.servicestring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicestringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServicestringApplication.class, args);
	}

}
